import pandas as pd
import streamlit as st
from plotly import express as px

from lib.common import (
    _,
    action_mapping,
    date_quickselect,
    date_select,
    door_opened,
    load_access_data,
    person_select,
    settings,
    unlock_actions,
)
from lib.menu import menu
from lib.streamlit_oauth import login


def plot_unlock_count(df):
    df = df[df["action"].isin(unlock_actions)]

    df_with_names = df.dropna(subset=["name"])
    action_count = (
        df_with_names.groupby("name")["action"].count().sort_values(ascending=False)
    )

    st.plotly_chart(
        px.bar(action_count, color=action_count.index, text_auto=True),
        use_container_width=True,
    )


def plot_general_actions_over_time(df, rolling_window=7):
    action_names_of_interest = [
        action_mapping[i] for i in range(1, 6)
    ]  # Include actions 1 to 5
    df_filtered = df[df["action_name"].isin(action_names_of_interest)]
    grouped_data = (
        df_filtered.groupby([pd.Grouper(key="date", freq="D"), "action_name"])
        .size()
        .unstack(fill_value=0)
    )
    smoothed_data = grouped_data.rolling(window=rolling_window, min_periods=1).mean()

    st.plotly_chart(px.line(smoothed_data), use_container_width=True)


def plot_actions_over_time_by_person(df, person_name):
    if person_name:
        df = df[df["name"] == person_name]
    df_person_grouped = (
        df.groupby([pd.Grouper(key="date", freq="D"), "action_name"])
        .size()
        .unstack(fill_value=0)
    )

    st.plotly_chart(px.bar(df_person_grouped), use_container_width=True)
    st.divider()
    st.dataframe(df_person_grouped)


def plot_door_opened_chart(df):
    # 240 is door open, maybe this makes sense? I dunno
    door_opened_df = df[df["action"].isin([door_opened] + unlock_actions)]

    # Create a copy of the DataFrame to avoid SettingWithCopyWarning
    door_opened_df = door_opened_df.copy()

    # extract hour from date
    door_opened_df.loc[:, "hour"] = door_opened_df["date"].dt.hour
    door_opened_df.loc[:, "weekday"] = door_opened_df["date"].dt.weekday

    hourly_open_count = door_opened_df.groupby("hour").size()

    # fig = px.area(hourly_open_count, labels={"value": "Open Count", "hour": "Hour"})
    fig = px.bar(
        hourly_open_count,
        labels={"value": _("Open Count"), "hour": _("Hour")},
        color=hourly_open_count.values,
    )
    st.plotly_chart(fig, use_container_width=True)

    # plot weekday
    weekday_open_count = door_opened_df.groupby("weekday").size()

    # map weekday number to weekday name
    weekday_open_count.index = weekday_open_count.index.map(
        {
            0: _("Monday"),
            1: _("Tuesday"),
            2: _("Wednesday"),
            3: _("Thursday"),
            4: _("Friday"),
            5: _("Saturday"),
            6: _("Sunday"),
        }
    )

    fig = px.bar(
        weekday_open_count,
        labels={"value": _("Open Count"), "weekday": _("Weekday")},
        color=weekday_open_count.values,
    )
    st.plotly_chart(fig, use_container_width=True)


def most_unlock_actions(df):
    lock_lockngo_df = df[df["action"].isin(unlock_actions)]
    lock_lockngo_count = (
        lock_lockngo_df.groupby("name")["action"].count().sort_values(ascending=False)
    )

    top_lock_lockngo = lock_lockngo_count.head(1)
    top_name, top_count = top_lock_lockngo.index[0], top_lock_lockngo.iloc[0]

    return top_name, top_count


def absteige_closed_days(df):
    unlock_unlatch_opened_df = df[df["action"].isin(unlock_actions)]
    daily_open_count_alternate = unlock_unlatch_opened_df.groupby(
        unlock_unlatch_opened_df["date"].dt.date
    ).size()
    total_days = (df["date"].dt.date.max() - df["date"].dt.date.min()).days + 1

    return total_days - daily_open_count_alternate.count()


def show_plots():
    df = load_access_data()

    date_start, date_end = date_select(df)

    # filter dataframe for date range
    df = df[(df["date"] >= date_start) & (df["date"] <= date_end)]

    # Top Unlocker
    top_unlocker, top_unlock_count = most_unlock_actions(df)
    st.info(
        _(
            "Who unlocked the door most often? **{top_unlocker}**, having done so **{top_unlock_count}** times."
        ).format(top_unlocker=top_unlocker, top_unlock_count=top_unlock_count),
        icon="🔥",
    )

    st.subheader(_("Unlock Count per Person"))
    plot_unlock_count(df)

    # When is it usually open
    st.subheader(
        _("What time is {common_name} usually open?").format(common_name=settings.name)
    )
    plot_door_opened_chart(df)

    st.subheader(_("All Actions over Time"))
    plot_general_actions_over_time(df)

    st.subheader(_("Actions per Person"))
    selected_name = st.session_state.get("selected_name", None)
    st.write(
        _("Actions Over Time for {selected_name}").format(
            selected_name=selected_name or "All"
        )
    )
    plot_actions_over_time_by_person(df, selected_name)


# Streamlit app starts here
title = _("{common_name} Nuki Analytics").format(common_name=settings.name)
st.set_page_config(page_title=title, layout="wide", page_icon="📈")
st.title(title)

menu()

with st.sidebar:
    person_select()
    date_quickselect()

is_user_logged_in = login(__file__)
if is_user_logged_in:
    show_plots()
