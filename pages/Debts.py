import pandas as pd
import plotly.graph_objects as go
import streamlit as st
from plotly import express as px

from lib.common import (
    AmountType,
    _,
    date_select,
    load_money_data,
    person_select,
    settings,
)
from lib.menu import menu
from lib.streamlit_oauth import login

df = load_money_data()


def money_chart_plot(df):
    df = df.sort_values(by="date", ascending=True)
    money_chart = px.bar(
        df,
        x="date",
        y="amount",
        color="name",
        text_auto=True,
        barmode="overlay",
        # hover_name=["name", "amount", "category"],
        hover_data={
            "name": True,
            "date": True,  # "|%B %d, %Y",
            "amount": ":.2f",
            "category": True,
            "comment": True,
        },
    )
    money_chart.update_xaxes(
        type="category",
        showgrid=True,
        ticks="outside",
        tickson="boundaries",
        categoryorder="category ascending",
    )

    return money_chart


def money_chart_category(df, show_comments: bool = False):
    money_hist = px.histogram(
        df,
        x="category",
        y="amount",
        color="comment" if show_comments else None,
        text_auto=True,
        hover_data={"amount": ":.2f", "comment": True},
    )
    df_sum = df.groupby("category")["amount"].sum().sort_values(ascending=False)
    if show_comments:
        money_hist.add_trace(
            go.Scatter(
                x=df_sum.index,
                y=df_sum.values,
                mode="markers+text",
                marker_size=13,
                textfont_size=14,
                text=df_sum.values,
                textposition="top center",
                name="Sum",
            )
        )
    return money_hist


def money_chart_group_date(df, categories=False, nbins=8):
    money_hist = px.histogram(
        df,
        x="date",
        y="amount",
        color="category" if categories else None,
        # text_auto=True,
        hover_data={"amount": ":.2f", "comment": True},
        text_auto=True,
        nbins=nbins,
    )
    money_hist.update_layout(bargap=0.2)
    money_hist.update_xaxes(
        showgrid=True, ticks="outside", tickson="boundaries", tickformat="%Y-%m-%d"
    )

    return money_hist


def show_debts(df):
    """Show debts per person"""
    amount_sum_by_name = df.dropna(subset=["name"])
    # remove Vereinskonto and Lenkerbande Shop
    amount_sum_by_name = amount_sum_by_name[
        ~amount_sum_by_name["name"].isin(["Vereinskonto", "Lenkerbande Shop"])
    ]
    amount_sum_by_name = (
        amount_sum_by_name.groupby("name")["amount"].sum().sort_values(ascending=False)
    )
    # filter out entries where amount is zero
    amount_sum = amount_sum_by_name[amount_sum_by_name != 0]

    max_debt = settings.money.max_debt

    st.write(
        _(
            "Positive values mean that you owe money, which you should send to the club's "
            "account when it exceeds {max_debt} €. Negative values mean that the club owes you money."
        ).format(max_debt=max_debt)
    )

    tab1, tab2, tab3 = st.tabs(
        [
            _("People who should have less debt"),
            _("All People with debt or credit"),
            _("Raw Data"),
        ]
    )

    # filter for amounts > 100
    amount_sum_by_name_large = amount_sum_by_name[amount_sum_by_name.values > max_debt]
    tab1.plotly_chart(
        px.bar(
            amount_sum_by_name_large,
            color=amount_sum_by_name_large.index,
            text_auto=True,
        ),
        use_container_width=True,
    )
    tab2.plotly_chart(
        px.bar(amount_sum, color=amount_sum.index, text_auto=True),
        use_container_width=True,
    )
    tab3.dataframe(amount_sum_by_name)

    col1, col2 = st.columns(2)
    selected_name = st.session_state.get("selected_name", None) or st.session_state.get(
        "username", None
    )
    with col1:
        person_select()
    if selected_name:
        my_debts = amount_sum_by_name[amount_sum_by_name.index == selected_name]
        # check that my_debts is not an empty series
        if not my_debts.empty:
            amount = my_debts.iloc[0]
            color = "green" if amount < 0 else "red"
            desc = _("Debt") if amount >= 0 else _("Credit")
            col2.metric(
                f":{color}[{desc} {selected_name}]",
                f"{amount:,.2f} €",
            )


def show_money_plots(df):
    if df.empty:
        return

    df = df.copy()

    selected_name = st.session_state.get("selected_name", None)
    if selected_name:
        # filter dataframe for selected_name
        df = df[df["name"] == selected_name]
    else:
        # exclude all repayments from df
        df = df[df["amount_type"] != AmountType.REPAYMENT.value]

    # replace column "date" with this date_column
    df["date"] = df["date"].dt.date

    col1, col2, col3 = st.columns(3)
    col1.metric(f":rainbow[{_('Total Balance')}]", f"{df['amount'].sum():,.2f} €")
    col2.metric(
        f":green[{_('Debts')}]", f"{df[df['amount'] > 0]['amount'].sum():,.2f} €"
    )
    col3.metric(
        f":red[{_('Credits')}]", f"{df[df['amount'] < 0]['amount'].sum():,.2f} €"
    )

    st.subheader(_("Money Distribution"))

    col1, col2, col3 = st.columns(3)

    categories = col1.toggle(_("Show categories"), value=True)

    limit_by = col2.radio(
        _("Filter by amount"), (_("All"), _("Income"), _("Expenses")), horizontal=True
    )
    if limit_by == _("Income"):
        df = df[df["amount"] > 0]
    elif limit_by == _("Expenses"):
        df = df[df["amount"] < 0]

    # count the number of months in df["date"]
    group_by = col3.radio(
        _("Group by"),
        (_("Week"), _("Month"), _("Quarter"), _("Year")),
        horizontal=True,
        index=1,
    )
    group_by_key = {
        _("Week"): 7,
        _("Month"): 30,
        _("Quarter"): 180,
        _("Year"): 365,
    }.get(group_by, 30)
    nbins = ((df["date"].max() - df["date"].min()).days // group_by_key) + 1

    tab1, tab2, tab3 = st.tabs([_("Aggregated"), _("Daily Entries"), _("Raw Data")])
    tab1.plotly_chart(
        money_chart_group_date(df, categories=categories, nbins=nbins),
        use_container_width=True,
    )
    tab2.plotly_chart(money_chart_plot(df), use_container_width=True)
    tab3.dataframe(df)

    st.subheader(_("Money Distribution by Category"))
    comments = st.toggle(_("Show comments"), value=False)
    st.plotly_chart(
        money_chart_category(df, show_comments=comments), use_container_width=True
    )


def show_plots(df):
    # show debts
    show_debts(df)

    date_start, date_end = date_select(df, show_quickselect=True)

    # filter dataframe for date range
    df = df[(df["date"] >= date_start) & (df["date"] <= date_end)]

    # select for categories
    if st.session_state.get("selected_categories", None):
        df = df[df["category"].isin(st.session_state["selected_categories"])]

    show_money_plots(df)


# Streamlit app starts here
title = _("{common_name} Debts").format(common_name=settings.name)
st.set_page_config(page_title=title, layout="wide", page_icon="💸")
st.title(title)

menu()

is_user_logged_in = login(__file__)
if is_user_logged_in:
    show_plots(df)
