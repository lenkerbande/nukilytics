import datetime

import pandas as pd
import streamlit as st

from lib.common import (
    AmountType,
    _,
    get_users,
    load_bank_data,
    load_money_data,
    settings,
)
from lib.menu import menu
from lib.streamlit_oauth import login

regular_amount_threshold = 30
recent_payment_days = 90


def show_plots():
    users = get_users()

    df_debts = load_money_data().copy()

    df_verein_bank = load_bank_data(settings.money.verein_iban)
    if df_verein_bank is None:
        st.error(_("No data available"))
        return

    regular_payments, irregular_payments = [], []
    user_payments = {}

    for user in users:
        bank_names = user["attributes"].get("bank", "")
        if isinstance(bank_names, str):
            bank_names = [bank_names]

        # multiply amounts by this factor data if attribute is set
        # otherwise payments show up duplicates for some users which are just paying for one account
        bank_factor = user["attributes"].get("bank_factor", 1)

        df_user = df_verein_bank[df_verein_bank["debtorName"].isin(bank_names)]

        df_user = df_user[df_user["amount"] > 0]
        df_user = df_user.sort_values("bookingDate", ascending=False)

        user_payments[user["name"]] = df_user

        df_user_regular = df_user[df_user["amount"] < regular_amount_threshold]

        last_payment_date = df_user_regular["bookingDate"].max()
        last_payment_days = (
            int((pd.Timestamp.now().date() - last_payment_date).days)
            if not pd.isna(last_payment_date)
            else None
        )

        last_payment_amount = (
            df_user_regular["amount"].iloc[0] if not df_user_regular.empty else 0
        )

        if last_payment_days is not None and last_payment_days < recent_payment_days:
            data = {
                "Name": user["name"],
                "last_payment_days": last_payment_days,
                "last_payment_amount": last_payment_amount * bank_factor,
                "newest_entry": last_payment_date,
                "oldest_entry": df_user_regular["bookingDate"].min(),
                #                    "average_amount": df_user["amount"].mean(),
                "sum_amount": df_user_regular["amount"].sum(),
                "count_amount": df_user_regular["amount"].count(),
                "debts": df_debts[df_debts["name"] == user["name"]]["amount"].sum(),
            }
            regular_payments.append(data)
        else:
            # get bank data without limit of amounts
            last_payment_date = df_user["bookingDate"].max()
            last_payment_days = (
                int((pd.Timestamp.now().date() - last_payment_date).days)
                if not pd.isna(last_payment_date)
                else None
            )

            last_payment_amount = df_user["amount"].iloc[0] if not df_user.empty else 0

            data = {
                "Name": user["name"],
                "last_payment_days": last_payment_days,
                "last_payment_amount": last_payment_amount * bank_factor,
                "newest_entry": last_payment_date,
                "oldest_entry": df_user["bookingDate"].min(),
                #                    "average_amount": df_user["amount"].mean(),
                "sum_amount": df_user["amount"].sum(),
                "count_amount": df_user["amount"].count(),
                "debts": df_debts[df_debts["name"] == user["name"]]["amount"].sum(),
            }

            irregular_payments.append(data)

    df_regular_payments = pd.DataFrame(regular_payments)

    col1, col2, col3, col4 = st.columns(4)
    col1.metric(_("Total members"), len(users))
    col2.metric(
        _("Number regular paying"),
        len(df_regular_payments),
        help=_("Members who have made a payment in the last 90 days"),
    )
    col3.metric(
        _("Monthly member payments"),
        "€ {:,.2f}".format(df_regular_payments["last_payment_amount"].sum()),
    )
    col4.metric(
        _("Last data entry"),
        df_verein_bank["bookingDate"].max().strftime("%Y-%m-%d"),
    )

    st.subheader(_("Regular paying members"))
    st.dataframe(
        df_regular_payments,
        column_config={
            "last_payment_days": st.column_config.ProgressColumn(
                _("Last Payment"),
                help=_("Days since the last payment"),
                format="%d",
                min_value=0,
                max_value=recent_payment_days,
            ),
            "last_payment_amount": st.column_config.ProgressColumn(
                _("Amount"),
                format="%d",
                min_value=0,
                max_value=df_regular_payments["last_payment_amount"].max(),
            ),
        },
        use_container_width=True,
        # height=len(df_regular_payments) * 40,
    )

    df_irregular_payments = pd.DataFrame(irregular_payments)
    st.subheader(_("Irregular or non paying members"))
    st.dataframe(
        df_irregular_payments,
        column_config={
            "last_payment_days": st.column_config.ProgressColumn(
                _("Last Payment"),
                help=_("Days since the last payment"),
                format="%d",
                min_value=0,
                max_value=int(df_irregular_payments["last_payment_days"].max()),
            ),
            "last_payment_amount": st.column_config.ProgressColumn(
                _("Amount"),
                format="%d",
                min_value=0,
                max_value=df_irregular_payments["last_payment_amount"].max(),
            ),
        },
        use_container_width=True,
        # height=len(df_regular_payments) * 40,
    )

    st.subheader(_("Repayment checks"))
    # replace column "date" with this date_column
    df_debts["date"] = df_debts["date"].dt.date

    df_repayments = df_debts[df_debts["amount_type"] == AmountType.REPAYMENT.value]
    df_repayments = df_repayments[df_repayments["date"] >= datetime.date(2021, 1, 1)]

    df_repayments = (
        df_repayments.drop(columns=["amount_type", "category", "comment", "timestamp"])
        .sort_values("date", ascending=False)
        .reset_index(drop=True)
    )

    # add column matching bank booking to df_repayments
    df_repayments["bank_data"] = None

    # iterate over each row of df_repayments and print the amount and date
    for index, row in df_repayments.iterrows():
        start_date = row["date"] - pd.Timedelta(days=5)
        end_date = row["date"] + pd.Timedelta(days=5)
        abs_amount = abs(row["amount"])

        if row["name"] not in user_payments:
            continue

        df_user = user_payments[row["name"]].copy()

        transactions_matching = df_user[
            (df_user["bookingDate"] >= start_date)
            & (df_user["bookingDate"] <= end_date)
            & (df_user["amount"] >= abs_amount - 5)
            & (df_user["amount"] <= abs_amount + 5)
        ]

        if not transactions_matching.empty:
            df_repayments.at[index, "bank_data"] = ", ".join(
                [
                    f"{r['bookingDate']}: {r['amount']} ({r['remittanceInformationUnstructured']})"
                    for i, r in transactions_matching.iterrows()
                ]
            )

    st.dataframe(df_repayments, use_container_width=True)


# Streamlit app starts here
title = _("{common_name} Members").format(common_name=settings.name)
st.set_page_config(page_title=title, layout="wide", page_icon="👯")
st.title(title)

menu()

is_user_logged_in = login(__file__, require_groups=[settings.auth.board_group_name])
if is_user_logged_in:
    show_plots()
