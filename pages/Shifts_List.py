import locale
import time

import pandas as pd
import streamlit as st

from lib.common import (
    _,
    get_user_names,
    git_update_file,
    load_shifts_from_file,
    settings,
)
from lib.menu import menu
from lib.streamlit_oauth import login


def save_shifts_to_file(df):
    # store edited_df to csv and to store
    df = df.sort_values("date", ascending=False)
    df.to_csv(str(settings.shifts_file_path), index=False)
    git_update_file(
        str(settings.shifts_file_path),
        commit_message="Updated shifts list by "
        + st.session_state.get("username", "unknown"),
    )


def show_list():
    # show_all_names = st.toggle(
    #     _("Show all names"), value=False, help=_("Default just show recent names")
    # )
    # recent_names = get_names(show_all=show_all_names)
    member_names = get_user_names()

    df = load_shifts_from_file()

    edited_df = st.data_editor(
        df,
        column_config={
            "date": st.column_config.DateColumn(
                _("Date") + " 📅",
                help=_("Day of shift"),
                required=True,
                format="ddd, DD.MM.YYYY",
            ),
            "primary": st.column_config.SelectboxColumn(
                _("Primary") + " ⭐⭐⭐",
                options=member_names,
            ),
            "secondary": st.column_config.SelectboxColumn(
                _("Secondary") + " ⭐⭐",
                options=member_names,
            ),
            "tertiary": st.column_config.SelectboxColumn(
                _("Tertiary") + " ⭐",
                options=member_names,
            ),
        },
        hide_index=True,
        num_rows="dynamic",
    )

    col1, col2 = st.columns(2)

    if col1.button(_("Save")):
        save_shifts_to_file(edited_df)

    if col2.button(_("Add month")):
        # add dates for days_of_shift on top of df
        max_date = edited_df["date"].max()

        first_day = (
            (max_date + pd.Timedelta(days=27))
            if pd.notna(max_date)
            else pd.Timestamp.now()
        ).replace(day=1, hour=0, minute=0, second=0)
        current_day = first_day

        current_locale = locale.getlocale()
        try:
            locale.setlocale(locale.LC_ALL, ("en_US", "UTF-8"))
        except locale.Error:
            st.warning("Unable to set locale: " + ("en_US", "UTF-8"))

        days_of_shift_int = [
            time.strptime(day_name, "%A").tm_wday for day_name in settings.days_of_shift
        ]
        locale.setlocale(locale.LC_ALL, current_locale)

        # Iterate over the dates of the current month
        new_dates = []
        while current_day.month == first_day.month:
            if current_day.weekday() in days_of_shift_int:
                new_dates.append(current_day)
            current_day += pd.Timedelta(days=1)

        # add new dates to df
        edited_df = pd.concat(
            [pd.DataFrame({"date": new_dates}), edited_df],
            ignore_index=True,
        ).sort_values("date", ascending=False)
        save_shifts_to_file(edited_df)
        st.rerun()


# Streamlit app starts here
title = _("{common_name} Shifts List").format(common_name=settings.name)
st.set_page_config(page_title=title, layout="wide", page_icon="💪")
st.title(title)

menu()

is_user_logged_in = login(__file__)
if is_user_logged_in:
    show_list()
