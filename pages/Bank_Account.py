import base64
import json
from itertools import chain
from os import name

import pandas as pd
import requests
import streamlit as st
from plotly import express as px

from lib.common import (
    _,
    date_select,
    get_user_banks,
    get_users,
    load_bank_data,
    settings,
)
from lib.menu import menu
from lib.streamlit_oauth import get_base_url, login


def show_account(iban: str, date_start, date_end, start_amount: float = 0):

    df = load_bank_data(iban)
    if df is None or df.empty:
        st.error(_("No data found, please upload some files"))
        return

    col1, col2 = st.columns(2)
    col1.metric(_("Balance"), f"{df['amount'].sum() + start_amount:,.2f} €")
    col2.metric(_("Last booking Date"), df["bookingDate"].max().strftime("%Y-%m-%d"))

    df = df[
        (df["bookingDate"] >= date_start.date())
        & (df["bookingDate"] <= date_end.date())
    ]

    col1, col2, col3 = st.columns(3)
    col1.metric(f"Total", f"{df['amount'].sum():,.2f} €")
    col2.metric(
        f":green[{_('Total Credit')}]", f"{df[df['amount'] > 0]['amount'].sum():,.2f} €"
    )
    col3.metric(
        f":red[{_('Total Expenses')}]", f"{df[df['amount'] < 0]['amount'].sum():,.2f} €"
    )

    total = df.groupby("Year")["amount"].sum().reset_index()
    einnahmen = (
        df[df["amount"] > 0]
        .groupby("Year")["amount"]
        .sum()
        .reset_index()
        .rename(columns={"amount": "Einnahmen"})
    )
    ausgaben = (
        df[df["amount"] < 0]
        .groupby("Year")["amount"]
        .sum()
        .reset_index()
        .rename(columns={"amount": "Ausgaben"})
    )

    # Merge the DataFrames on the 'bookingDate' column
    df_yearly_evaluation = total.merge(einnahmen, on="Year", how="outer").merge(
        ausgaben, on="Year", how="outer"
    )

    # make the column "Year" a string column
    df_yearly_evaluation["Year"] = df_yearly_evaluation["Year"].astype(str)

    # Set 'bookingDate' as the index again, if required
    df_yearly_evaluation.set_index("Year", inplace=True)

    st.dataframe(df_yearly_evaluation)

    # plot df_yearly_evaluation
    st.plotly_chart(
        px.bar(
            df_yearly_evaluation,
            # color=df_yearly_evaluation.index,
            # text=df_yearly_evaluation.index,
            text_auto=True,
            barmode="overlay",
        )
    )

    col1, col2 = st.columns(2)
    search = col1.text_input("Search:", key="search" + str(iban))

    min_amount = df["amount"].min()
    max_amount = df["amount"].max()
    values = col2.slider(
        "Limit amounts", min_amount, max_amount, (min_amount, max_amount)
    )

    df = df[(df["amount"] >= values[0]) & (df["amount"] <= values[1])]

    # reorganize columns
    df = df[
        [
            "bookingDate",
            "name",
            "amount",
            "remittanceInformationUnstructured",
            "iban",
            "creditorName",
            "debtorName",
            "creditorIban",
            "debtorIban",
        ]
    ]

    if search:
        # search df in fields "Buchungstext" and "Partnername"
        df = df[
            df["name"].str.contains(search, case=False, na=False)
            | df["remittanceInformationUnstructured"].str.contains(
                search, case=False, na=False
            )
            # | df["Buchungs-Details"].str.contains(search, case=False, na=False)
            # | df["Zahlungsreferenz"].str.contains(search, case=False, na=False)
        ]

    st.dataframe(
        df,
        column_config={
            "Partner Kontonummer": st.column_config.NumberColumn("iban", format="%d"),
        },
        use_container_width=True,
    )

    # get absteige users and their data
    abs_partnernames = get_user_banks() | set(settings.money.absteige_creditors)
    abs_partnernames -= {"Rainer Neuroth"}

    radwg_partnernames = {
        "DI Andrea Mann",
        "Dr. Friedrich Josef Lackinger",
        "Mag.iur. Gregor Höpler",
        "Mag. Harald Siegmund",
        "Ing. Patrick Weise",
        "Claudia Krammer",
        "Rometsch Elfriede,Dr.Heinrich M",
        "Thomas Kreillechner",
        "Ing. Ulrich Wexberg",
        "Julius Schwestka",
        "Eva Westhauser",  # vermieterin
    }

    df_rest = df[~df["name"].isin(abs_partnernames | radwg_partnernames)]

    st.subheader("Payments excluding Absteige and RadWG")
    # group by "Partnername" and sum "amount"
    df_partner = df_rest.groupby("name")["amount"].sum().reset_index()

    def plot_pie(df, title):
        df.loc[:, "amount"] = df["amount"].abs()
        st.plotly_chart(
            px.pie(
                df,
                values="amount",
                names="name",
                title=title,
            )
        )

    col1, col2 = st.columns(2)
    with col1:
        plot_pie(
            df_rest[df_rest["amount"] > 0]
            .groupby("name")["amount"]
            .sum()
            .reset_index(),
            "Positive Amounts",
        )
    with col2:
        plot_pie(
            df_rest[df_rest["amount"] < 0]
            .groupby("name")["amount"]
            .sum()
            .reset_index(),
            "Negative Amounts",
        )

    with st.expander("Raw data"):
        st.dataframe(df_partner)

    # st.plotly_chart(
    #     px.histogram(
    #         df_rest[df_rest["amount"] > 0],
    #         x="name",
    #         y="amount",
    #         # log_y=True,
    #         # color="Buchungs-Details",
    #         text_auto=True,
    #         # hover_data={"amount": ":.2f", "Buchungs-Details": True},
    #         # nbins=20,
    #     ).update_xaxes(categoryorder="total ascending"),
    #     use_container_width=True,
    # )

    # st.plotly_chart(
    #     px.histogram(
    #         df_rest[df_rest["amount"] < 0],
    #         x="name",
    #         y="amount",
    #         # color="Buchungs-Details",
    #         text_auto=True,
    #         # hover_data={"amount": ":.2f", "Buchungs-Details": True},
    #         nbins=20,
    #     ).update_xaxes(categoryorder="total ascending"),
    #     use_container_width=True,
    # )

    st.subheader("Absteige")

    # get all rows where "Partnername" is in bank_partnernames
    df_absteige = df[df["name"].isin(abs_partnernames)]

    df_absteige_grouped = df_absteige.groupby("name")["amount"].sum().reset_index()

    col1, col2 = st.columns(2)
    with col1:
        plot_pie(
            df_absteige_grouped[df_absteige_grouped["amount"] > 0], "Positive Amounts"
        )
    with col2:
        plot_pie(
            df_absteige_grouped[df_absteige_grouped["amount"] < 0], "Negative Amounts"
        )

    with st.expander("Raw data"):
        st.dataframe(
            df_absteige,
            column_config={
                "Partner Kontonummer": st.column_config.NumberColumn(
                    "name", format="%d"
                ),
            },
            use_container_width=True,
        )


def show_bank_refresh_button(data: dict):
    """Show button to fetch new bank data"""
    credentials = data.get("bankproxy_credentials")
    if not credentials:
        return

    auth = base64.b64encode(
        f"{credentials['clientId']}:{credentials['clientSecret']}".encode()
    ).decode()

    iban = data.get("iban")
    df = load_bank_data(iban)
    if df is None or df.empty:
        date_limits = {}
    else:
        # get row of the latest booking date
        newest_row = df[df["bookingDate"] == df["bookingDate"].max()].iloc[0]

        date_limits = {
            "dateFrom": newest_row["bookingDate"].strftime("%Y-%m-%d"),
            "dateTo": "9999-12-31",
            "entryReferenceFrom": newest_row["entryReference"],
        }

    base_url = get_base_url()
    response = requests.post(
        settings.money.bankproxy_url,
        headers={"Authorization": f"Basic {auth}"},
        json={
            "callbackUri": f"{base_url}/Bank_Data_Load?iban={iban}",
            "accounts": [
                {
                    "iban": iban,
                }
                | date_limits
            ],
        },
        allow_redirects=False,
    )

    if response.status_code != 302:
        st.error(response.text)
        return

    name = data.get("name")
    container = f"""
        <a target="_blank" href="{response.headers.get('Location')}">
            Click to refresh bank data for {name} ({iban})
        </a>
    """
    st.markdown(container, unsafe_allow_html=True)


def show_plots():
    bank_accounts_file = settings.money.bank_accounts_path
    bank_accounts = {}

    col1, col2 = st.columns(2)

    with col1:
        date_start, date_end = date_select(
            None,
            date_start_default=pd.to_datetime("now", utc=True)
            - pd.Timedelta(days=365 * 5),
            show_quickselect=True,
        )

        if bank_accounts_file.exists():
            with bank_accounts_file.open() as f:
                bank_accounts = json.load(f)

            split_by_accounts = st.toggle(_("Split by accounts/folders"), value=False)
        else:
            split_by_accounts = False

    uploaded_file = col2.file_uploader(
        _("Upload new bank data file(s)"),
        type="csv",
        accept_multiple_files=False,
    )

    if bank_accounts and col2.button(_("Refresh bank data")):
        for name, data in bank_accounts.items():
            show_bank_refresh_button(data | {"name": name})
        return

    bank_data_path = settings.money.bank_data_path

    if uploaded_file:
        bytes_data = uploaded_file.read()
        timestamp = pd.Timestamp.now().strftime("%Y%m%d_%H%M%S_")
        with open(bank_data_path / (timestamp + uploaded_file.name), "wb") as f:
            f.write(bytes_data)

    if not bank_data_path.exists():
        st.error("No bank data folders found")
        return

    if split_by_accounts:
        for name, data in bank_accounts.items():
            iban = data["iban"]
            start_amount = data.get("start_amount", 0)
            with st.expander(name + " - " + iban):
                show_account(iban, date_start, date_end, start_amount)
    else:
        st.subheader(_("All accounts combined"))
        start_amount = sum(
            [data.get("start_amount", 0) for data in bank_accounts.values()]
        )
        show_account(None, date_start, date_end, start_amount)


# Streamlit app starts here
title = _("Lenkerbande Bank Analytics").format(common_name=settings.name)
st.set_page_config(page_title=title, layout="wide", page_icon="🏦")
st.title(title)

menu()

is_user_logged_in = login(__file__, require_groups=[settings.auth.board_group_name])
if is_user_logged_in:
    show_plots()
