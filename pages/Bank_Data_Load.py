import base64
import json
from urllib.parse import unquote, urljoin

import pandas as pd
import requests
import streamlit as st

from lib.common import _, settings
from lib.menu import menu
from lib.streamlit_oauth import login

# Streamlit app starts here
title = _("{common_name} Bank Data Load").format(common_name=settings.name)
st.set_page_config(page_title=title, page_icon=":bank:")
st.title(title)

result = unquote(st.query_params["result"])
iban = st.query_params["iban"]

data_url = urljoin(settings.money.bankproxy_url, result)

with settings.money.bank_accounts_path.open() as f:
    bank_accounts = json.load(f)

credentials = [x for x in bank_accounts.values() if x["iban"] == iban][0][
    "bankproxy_credentials"
]
auth = base64.b64encode(
    f"{credentials['clientId']}:{credentials['clientSecret']}".encode()
).decode()

response = requests.get(data_url, headers={"Authorization": f"Basic {auth}"})

if response.status_code != 200:
    st.error(_("Error loading data from bank"))
    st.error(response.text)
    st.stop()
else:
    data = response.json()
    transactions = data["result"][0]["transactions"]["booked"]

    if not transactions:
        st.error(_("No transactions found"))
        st.stop()

    fields = [
        "transactionId",
        "entryReference",
        "valueDate",
        "bookingDate",
        "debtorName",
        "creditorName",
        "remittanceInformationUnstructured",
    ]

    converted_data = []
    for t in transactions:
        d = {field: t[field] for field in fields if field in t}
        d["amount"] = t["transactionAmount"]["amount"]
        d["debtorIban"] = t["debtorAccount"]["iban"]
        d["creditorIban"] = t["creditorAccount"]["iban"]
        converted_data.append(d)

    df = pd.DataFrame(converted_data)

    # save to disk
    save_path = settings.money.bank_data_path / f"{iban}.csv"

    if save_path.exists():
        df = pd.concat([pd.read_csv(str(save_path)), df], ignore_index=True)
        df.drop_duplicates(inplace=True)

    df.to_csv(str(save_path), index=False)
    st.success(_("Bank data saved to disk"))

    st.success(_("You can close this window now"))

    st.stop()
