import json
import os

import pandas as pd
import streamlit as st
from fpdf import FPDF

from lib.common import (
    AmountType,
    Category,
    _,
    get_user_names,
    load_access_data,
    load_money_data,
    save_money_data,
    settings,
)
from lib.menu import menu
from lib.streamlit_oauth import login

pdf_file_path = "dynamic_name_table.pdf"


def generate_pdf(names):
    names_width = 30
    row_height = 5
    num_cols = 60
    max_rows = 37
    font = "Helvetica"

    # Initialize PDF
    pdf = FPDF(orientation="landscape", format="A4")
    pdf.add_page()
    pdf.set_margin(10)
    pdf.set_font(font, size=9)

    # Adding table header
    pdf.set_font(font, style="B")
    pdf.cell(names_width, row_height, "Name", border=1)
    for i in range(
        1, int(num_cols / 5) + 1
    ):  # Assuming 5 additional columns for demonstration
        pdf.cell(20, row_height, f"{i*5}", border=1, align="R")
        pdf.cell(0.5, row_height, "", border=0, fill=1)
    pdf.ln()

    pdf.set_font(font)

    # copy names list
    names = names.copy() + [""] * (max_rows - len(names))

    # Adding names and empty columns to the table
    for rownum, name in enumerate(names):
        pdf.set_fill_color(220, 220, 220)
        pdf.cell(names_width, row_height, name, border=1, fill=rownum % 2 == 0)
        for i in range(num_cols):  # Matching the number of additional columns
            pdf.set_fill_color(220, 220, 220)
            pdf.cell(4, row_height, "", border=1, fill=rownum % 2 == 0)
            if (i + 1) % 5 == 0:
                pdf.set_fill_color(0, 0, 0)
                pdf.cell(0.5, row_height, "", border=0, fill=1)
        pdf.ln()

    # Save the PDF
    output_path = pdf_file_path
    pdf.output(output_path)


def load_name_data() -> dict:
    """Load names configuration from local files and return them"""
    if settings.names_json_path.exists():
        with open(settings.names_json_path, "r") as f:
            names = json.load(f)
    else:
        names = {}

    return names


def save_name_data(names: dict):
    """Save names configuration from session state to local files"""
    with open(settings.names_json_path, "w") as f:
        json.dump(names, f)


def get_money_df():
    # get data
    beer_df = st.session_state["beer_df"].copy()
    start_month = st.session_state["start_month"]
    end_month = st.session_state["end_month"]
    beer_price = st.session_state["beer_price"]
    timestamp = st.session_state["timestamp"]

    beer_df = beer_df[beer_df["amount of drinks"] != 0]

    # add category
    beer_df["category"] = Category.BEER.value
    beer_df["amount"] = beer_df["amount of drinks"] * beer_price

    # add date
    date_range = pd.date_range(start_month, end_month, freq="ME")

    beer_df.loc[:, "amount_fraction"] = beer_df["amount"] / len(date_range)

    for date in date_range:
        beer_df.loc[:, date] = beer_df["amount_fraction"]

    df_money = pd.melt(beer_df, id_vars="name", value_vars=date_range).dropna()
    df_money["date"] = df_money["variable"]
    df_money["amount"] = df_money["value"]
    df_money["amount_type"] = AmountType.DEBT.value
    df_money["category"] = Category.BEER.value
    df_money["comment"] = ""
    df_money["timestamp"] = timestamp

    # reorder columns
    df_money = df_money[
        [
            "name",
            "amount",
            "amount_type",
            "date",
            "category",
            "comment",
            "timestamp",
        ]
    ]
    return df_money


def show_confirm_dialog():
    df_money = get_money_df()

    st.warning(_("Following data will be sent:"))

    col1, col2 = st.columns(2)
    col1.write(_("Start Month: ") + str(st.session_state["start_month"]))
    col2.write(_("End Month: ") + str(st.session_state["end_month"]))

    # filter all rows with amount > 0
    beer_df = st.session_state["beer_df"]
    beer_df = beer_df[beer_df["amount of drinks"] > 0]
    st.table(beer_df)

    st.dataframe(df_money)

    st.session_state["df_money"] = df_money
    st.session_state["beer_price"] = st.session_state["beer_price"]

    col1, col2 = st.columns([1, 4])

    col1.button(_("Back"), on_click=lambda: st.session_state.pop("confirm_dialog"))
    col2.button(
        _("Confirm"),
        help=_("Really send data now"),
        type="primary",
        on_click=send_data,
        disabled=df_money.empty,
    )


def show_form():
    df = load_money_data()
    df_access = load_access_data()

    # get df dates newer than 90 days
    recent_date = pd.to_datetime(pd.Timestamp.now() - pd.Timedelta(days=180), utc=True)
    df = df[df["date"] > recent_date]
    df_access = df_access[df_access["date"] > recent_date]

    col1, col2 = st.columns(2)
    col1.date_input(
        _("Start Month"),
        max(df[(df["category"] == Category.BEER.value) & (df["amount"] > 0)]["date"]),
        key="start_month",
    )
    col2.date_input(_("End Month"), pd.to_datetime("now", utc=True), key="end_month")

    # beer price input
    st.number_input(
        _("Beer Price"), value=1.5, key="beer_price", step=0.1, disabled=True
    )

    edited_df = st.empty()

    st.info(_("Specify amount of drinks for each person (not amount in euro)"))

    col1, col2 = st.columns([1, 2])

    names = load_name_data()

    beer_list_file = col2.file_uploader(
        _("Image of beer list"),
        help=_("Picture of the original beer list"),
        type=["png", "jpg", "jpeg", "gif", "pdf"],
    )
    if beer_list_file is not None:
        uploaded_name = beer_list_file.name
        # get extension for uploaded_name
        uploaded_name, extension = os.path.splitext(uploaded_name)

        # To read file as bytes and store in session
        st.session_state["beer_list_data"] = {
            "data": beer_list_file.getvalue(),
            "extension": extension,
        }

    with col2.expander(_("Names configuration")):
        names_remove = st.text_input(
            _("Removed Names"),
            value=",".join(names.get("names_remove", [])),
        )
        names_add = st.text_input(
            _("Additional Names"),
            value=",".join(names.get("names_add", [])),
        )

    if names_remove != ",".join(names.get("names_remove", [])) or names_add != ",".join(
        names.get("names_add", [])
    ):
        names = {
            "names_remove": names_remove.split(","),
            "names_add": names_add.split(","),
        }
        save_name_data(names)

    user_names = get_user_names()

    names = list(
        set(user_names) - set(names_remove.split(","))
        | set(names_add.split(",")) - {""}
    )

    # duplicate row with name "Rainer"
    if "Rainer" in names:
        names.append("Rainer")
        # sort names
    names = sorted(names)

    generate_pdf(names)
    with open(pdf_file_path, "rb") as file:
        col2.download_button(
            label="Download Getränkeliste",
            data=file,
            file_name="getraenkeliste.pdf",
            mime="application/pdf",
        )

    names_df = pd.DataFrame({"name": names})

    # create a dataframe with names in rows and values in columns
    if "beer_df" not in st.session_state:
        names_df["amount of drinks"] = 0
    else:
        names_df["amount of drinks"] = st.session_state["beer_df"]["amount of drinks"]

    edited_df = col1.data_editor(
        names_df,
        num_rows="fixed",
        height=600,
        column_config={
            "name": st.column_config.TextColumn(_("Name"), disabled=True),
            "amount of drinks": st.column_config.NumberColumn(
                _("Amount of Drinks"),
                min_value=0,
                step=1,
                required=True,
            ),
        },
        hide_index=True,
    )

    st.button(
        _("Send Data"),
        type="primary",
        key="send_data",
        on_click=lambda: st.session_state.update(
            {
                "beer_df": edited_df,
                "confirm_dialog": True,
            }
        ),
    )


def send_data():
    df_money = st.session_state["df_money"]
    beer_price = st.session_state["beer_price"]

    msg = _("Beer formular sent") + ":\n\n"
    msg += "\n".join(
        f"{name}: {int(amount/beer_price)} drinks = {amount} €"
        for name, amount in df_money.groupby("name")["amount"].sum().items()
    )

    save_money_data(df_money, message=msg)

    if "beer_list_data" in st.session_state:
        bytes_data = st.session_state["beer_list_data"]["data"]
        extension = st.session_state["beer_list_data"]["extension"]

        beer_list_path = os.path.join(
            str(settings.data_dir_path),
            "beer",
            str(st.session_state["timestamp"]) + extension,
        )
        # create parent directory for beer_list_path
        os.makedirs(os.path.dirname(beer_list_path), exist_ok=True)
        with open(beer_list_path, "wb") as f:
            f.write(bytes_data)

    st.session_state["confirm_dialog"] = False
    st.session_state["send_data_done"] = True


def show_main():
    if "timestamp" not in st.session_state:
        st.session_state["timestamp"] = pd.Timestamp.now(tz=settings.timezone)

    if not st.session_state.get("confirm_dialog", False) and not st.session_state.get(
        "send_data_done", False
    ):
        show_form()

    # send data
    elif st.session_state.get("confirm_dialog", False):
        show_confirm_dialog()

    elif st.session_state.get("send_data_done", False):
        st.success(_("Beer data successfully sent"))
        if st.button("Reset"):
            st.session_state["send_data_done"] = False
            st.session_state["confirm_dialog"] = False


# Streamlit app starts here
# beer emoji
title = _("{common_name} Beer Form").format(common_name=settings.name)
st.set_page_config(page_title=title, page_icon="🍺")
st.title(title)

menu()

is_user_logged_in = login(__file__)
if is_user_logged_in:
    show_main()
