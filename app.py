import random
from datetime import datetime
from gettext import ngettext as _n

import pandas as pd
import streamlit as st

from lib.common import (
    _,
    available_languages,
    door_actions,
    load_access_data,
    lock_actions,
    no_actions,
    set_language,
    settings,
    unlock_actions,
)
from lib.menu import menu
from lib.nuki_datafetcher import get_smartlock_details, update_data
from lib.streamlit_oauth import load_user_data, login


def get_greeting():
    greetings = {
        "morning": [
            "Guten Morgen",
            "Morgen",
            "Schönen guten Morgen",
            "Einen wunderschönen guten Morgen",
            "Hallo, guten Morgen",
        ],
        "afternoon": [
            "Guten Tag",
            "Mahlzeit",
            "Moizeit",
            "Seas",
            "Grias di",
            "Einen wunderschönen guten Tag",
            "Hallo, guten Tag",
        ],
        "evening": [
            "Guten Abend",
            "Abend",
            "Seas",
            "Einen wunderschönen guten Abend",
            "Hallo, guten Abend",
        ],
        "night": [
            "Gute Nacht",
            "Nacht",
            "Schlaf gut",
            "Schönen guten Abend noch",
            "Hallo, gute Nacht",
        ],
    }

    current_hour = datetime.now().hour

    if 5 <= current_hour < 10:
        period = "morning"
    elif 10 <= current_hour < 16:
        period = "afternoon"
    elif 16 <= current_hour < 22:
        period = "evening"
    else:
        period = "night"

    greeting = random.choice(greetings[period])
    return greeting


def show_door_state(df):
    locked_and_unlocked_df = df[df["action"].isin(lock_actions + unlock_actions)]
    last_lock_row = locked_and_unlocked_df.iloc[0]

    if last_lock_row["action"] in unlock_actions:
        last_lock_action = st.success
        text = _("{common_name} is open since").format(common_name=settings.name)
        icon = "🔓"
    else:
        last_lock_action = st.error
        text = _("{common_name} is locked since").format(common_name=settings.name)
        icon = "🔒"

    # calculate duration since last action
    duration_since_last_action = pd.to_datetime("now", utc=True) - last_lock_row["date"]

    # format to localized string with strftime, skip the days if days are zero
    hours, remainder = divmod(duration_since_last_action.seconds, 3600)
    minutes, seconds = divmod(remainder, 60)
    formatted_hours = "{:02}:{:02} h".format(hours, minutes)

    if duration_since_last_action.days == 0:
        duration_since_last_action_str = formatted_hours
    else:
        duration_since_last_action_str = (
            _n(
                _("{days} day"), _("{days} days"), duration_since_last_action.days
            ).format(days=duration_since_last_action.days)
            + ", "
            + formatted_hours
        )

    last_lock_action(
        f"{text} {duration_since_last_action_str}"
        + (
            f", by **{last_lock_row['name']}**"
            if last_lock_row["name"] and not isinstance(last_lock_row["name"], float)
            else ""
        ),
        icon=icon,
    )


@st.cache_data(ttl="1h")
def get_battery_data():
    smartlock_data = get_smartlock_details()
    return smartlock_data.get("state", {})


def show_battery_warning():
    state_data = get_battery_data()

    if state_data.get("batteryCritical"):
        st.warning(
            _(
                "Battery of Nuki door lock is critical at just {battery_level} %!"
            ).format(
                common_name=settings.name, battery_level=state_data["batteryCharge"]
            )
        )

    if state_data.get("keypadBatteryCritical"):
        st.warning(_("Battery of keypad is critical!"))
    if state_data.get("doorsensorBatteryCritical"):
        st.warning(_("Battery of doorsensor is critical!"))


def show_battery_level():
    state_data = get_battery_data()
    if state_data:
        st.write(
            _("Nuki battery level is at {battery_level} %").format(
                battery_level=state_data["batteryCharge"]
            )
        )


def show_access_history(df):
    st.subheader(_("Access history"))
    show_door = st.toggle(_("Show door actions"), False, key="door_actions")

    # Open since
    actions_to_filter = lock_actions + unlock_actions + no_actions
    if show_door:
        actions_to_filter += door_actions

    # reduce columns to date, name, action_name
    df_display = df[df["action"].isin(actions_to_filter)]
    df_display = df_display[["date", "action_name", "name"]].copy()

    # rename columns of df_display
    # df_display = df_display.rename(
    #     columns={"date": _("Date"), "action_name": _("Action"), "name": "Name"}
    # )

    # map values of action_name to map unlatch to unlock
    df_display["action_name"] = df_display["action_name"].map(
        {
            "Unlatch": "📂 " + _("Unlatch"),
            "Lock": "🔒 " + _("Lock"),
            "Unlock": "🔓 " + _("Unlock"),
            "Door opened": "📖 " + _("Door opened"),
            "Door closed": "📕 " + _("Door closed"),
            "Lock'n'go": "🔒+🚶 " + _("Lock'n'go"),
            "Keypad unauthorized": "🚫 " + _("Keypad invalid code"),
        }
    )

    # set to fixed 20 entries for now
    amount_entries = int(st.session_state.get("amount_entries", 20))
    df_display = df_display.iloc[0:amount_entries]

    # style df_display, make all action_name "Unlatch" green background
    # df_display = df_display.style.apply(
    #     lambda x: [
    #         "background-color: white" if i % 2 == 0 else "background-color: #fafafa"
    #         for i in range(len(x))
    #     ]
    # )

    st.dataframe(
        df_display,
        hide_index=True,
        use_container_width=True,
        column_config={
            "date": st.column_config.DatetimeColumn(
                _("Date") + " 📅",
                format="ddd, DD. MMM HH:mm",
                timezone=settings.timezone,
            ),
            "name": st.column_config.TextColumn(_("Name")),
            "action_name": st.column_config.TextColumn(_("Action")),
        },
    )

    # st.selectbox(
    #     _("Amount of entries"), ("20", "50", "100"), key="amount_entries"
    # )


def show_next_shifts():
    # load df from shifts_file_path
    shifts_file_path = settings.shifts_file_path
    if not shifts_file_path.is_file():
        return

    # load csv, column date as timestamp, other columns as strings
    df = pd.read_csv(
        str(shifts_file_path),
        parse_dates=["date"],
        dtype={"primary": str, "secondary": str, "tertiary": str},
    )

    # get next upcoming date from df
    next_shift = df[df["date"] >= pd.Timestamp.now().floor("d")].sort_values(
        "date", ascending=True
    )

    if next_shift.empty:
        st.warning(_("No upcoming shifts found - please create some"))
        return

    next_shift = next_shift.iloc[0]
    if isinstance(next_shift["primary"], float):
        # no primary person set
        st.warning(
            _("No primary person set for next shift on **{next_shift_date}**").format(
                next_shift_date=next_shift["date"].strftime("%a, %d. %b")
            )
        )
    else:
        st.info(
            _("Next open day is **{date}**, which is hosted by **{primary}**").format(
                date=next_shift["date"].strftime("%a, %d. %b"),
                primary=next_shift["primary"],
            )
            + (
                _(" with **{secondary}**").format(secondary=next_shift["secondary"])
                if next_shift["secondary"]
                and not isinstance(next_shift["secondary"], float)
                else ""
            )
            + (
                _(" and **{tertiary}**").format(tertiary=next_shift["tertiary"])
                if next_shift["tertiary"]
                and not isinstance(next_shift["tertiary"], float)
                else ""
            )
        )


def show_dashboard():
    df = load_access_data()

    show_door_state(df)

    show_battery_warning()

    show_next_shifts()

    show_access_history(df)

    show_battery_level()


# Streamlit app starts here
title = _("{common_name} Data Dashboard").format(common_name=settings.name)
st.set_page_config(page_title=title, page_icon="🏠")

menu()
load_user_data()

if "user_data" in st.session_state:
    title = get_greeting() + ", " + st.session_state.user_data["name"]

st.title(title)

with st.sidebar:
    # if "language" not in st.session_state:
    #     st.session_state.language = get_browser_language() or default_language
    # language = st.session_state.language

    selected_language = st.selectbox(
        _("Language"),
        available_languages.values(),
        index=list(available_languages.keys()).index(st.session_state.language),
    )

    selected_language_key = {v: k for k, v in available_languages.items()}.get(
        selected_language
    )

    if selected_language_key != st.session_state.language:
        st.session_state.language = selected_language_key
        set_language(selected_language_key)
        st.rerun()

    login()


update_data()
show_dashboard()
