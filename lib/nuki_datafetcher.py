import pandas as pd
import requests

from lib.common import settings

page_size = 50
base_url = f"https://api.nuki.io/smartlock/log?limit={page_size}"


def get_smartlock_details(smartlock_id: int = None) -> dict:
    """Get smartlock details from Nuki API"""

    if smartlock_id is None:
        smartlock_id = settings.nuki.smartlock_id

    if settings.nuki.auth_header is None or smartlock_id is None:
        return {}

    response = requests.get(
        f"https://api.nuki.io/smartlock/{smartlock_id}",
        headers={"Authorization": settings.nuki.auth_header},
        timeout=10,
    )
    response.raise_for_status()

    return response.json()


def fetch_and_append(df_old: pd.DataFrame, to_date=None, from_date=None):
    """Fetch data from Nuki API and append to existing dataframe"""

    if settings.nuki.auth_header is None:
        print("Could not update nuki logs")
        return df_old, []

    def ensure_date_str(date):
        if not isinstance(date, str):
            return date.isoformat("T").replace("+00:00", ".000Z")
        return date

    url = base_url

    if to_date:
        url += f"&toDate={ensure_date_str(to_date)}"
    if from_date:
        url += f"&fromDate={ensure_date_str(from_date)}"

    # print(url)
    response = requests.get(
        url, headers={"Authorization": settings.nuki.auth_header}, timeout=10
    )
    response.raise_for_status()

    data = response.json()
    # print(len(data))

    if len(data) == 1 and data[0]["date"] == ensure_date_str(from_date):
        # no new data
        return df_old, data

    df = pd.DataFrame(data)
    if data:
        df = df.drop(
            columns=[
                "smartlockId",
                "deviceType",
                "trigger",
                "id",
                "autoUnlock",
                "source",
                "ajarTimeout",
            ],
            errors="ignore",
        )
        df["date"] = pd.to_datetime(df["date"])
        # set d["action"] to 300 where d["action"] is 3 and df["state"] is 254
        df.loc[(df["action"] == 3) & (df["state"] == 224), "action"] = 300

    return pd.concat([df_old, df]), data


def update_data():
    """Load existing nuki data and then append new data"""

    # update data only when modification date of data_file is older than 1 minute
    nuki_file_path = settings.nuki.file_path

    if (
        nuki_file_path.exists()
        and (pd.Timestamp.now().timestamp() - nuki_file_path.stat().st_mtime) < 60
    ):
        return

    to_date = pd.to_datetime("1970-01-01", utc=True)
    from_date = None

    if not nuki_file_path.exists():
        df = pd.DataFrame()
    else:
        df = pd.read_csv(str(nuki_file_path), parse_dates=["date"])
        df["date"] = pd.to_datetime(df["date"])
        from_date = df["date"].max()

    df, data = fetch_and_append(df, from_date=from_date)

    while data and len(data) == page_size and df["date"].iloc[-1] > to_date:
        # query again with date ranges
        df, data = fetch_and_append(df, to_date=data[-1]["date"], from_date=from_date)

    # reorder columns in order date, name, action
    df = df.reindex(columns=["date", "action", "name"])
    # order df by date
    df = df.sort_values(by="date", ascending=False)
    df.to_csv(str(nuki_file_path), index=False)
