# plenums-reminder

# Path: lib/reminder_money.py

import json
import random
from datetime import datetime, timedelta

import emoji_generator.random_emoji as emojigen

from lib.common import load_money_data, load_shifts_from_file, send_message, settings

repayment_sentences = [
    "Wann kannst du die offene Summe überweisen?",
    "Könnten wir über deine ausstehenden Schulden sprechen?",
    "Hast du das Geld vielleicht beim letzten Waschgang in der Hosentasche vergessen?",
    "Wie steht's mit einer Schatzsuche nach dem verlorenen Schatz (deiner Zahlung)?",
    "Es wird Zeit, dass du deine Schulden begleichst.",
    "Zahl jetzt, oder es gibt Ärger.",
    "Deine Schulden, deine Verantwortung. Jetzt zahlen!",
    "Schulden sind wie ein Kaugummi: Sie werden immer größer und kleben an dir fest.",
    "Es ist Zahltag, keine Versteckspielzeit. Wo bleibt das Geld?",
    "Du hast Schulden. Zeit, sie zu begleichen.",
    "Schulden sind wie ein Bumerang. Sie kommen immer zurück.",
    "Schulden sind wie ein Schatten. Sie folgen dir überallhin.",
    "Schulden sind wie ein schlechter Witz. Sie sind nicht lustig.",
    "Schulden sind wie ein schlechter Traum. Sie gehen nicht weg.",
    "Schulden sind wie ein schlechter Film. Sie enden nicht.",
    "Schulden sind wie ein schlechter Tag. Sie gehen nicht vorbei.",
    "Ein Mensch ohne Schulden ist wie ein Ozean ohne Wellen: ruhig, klar und frei von Turbulenzen.",
    "Schulden sind wie Gewichte an den Füßen; je mehr man hat, desto schwerer ist es, vorwärts zu kommen",
    "Schulden sind das Feuer, in dem oft die Zukunft verbrannt wird, um die Gegenwart zu wärmen",
    "Zu leihen ist ein Augenblickswerk, zurückzuzahlen eine Lebenskunst",
    "Zahlen ist wie Radfahren: Einmal im Schwung, kommt man leichter voran und stürzt weniger oft in die Schuldenfalle!",
    "Ein Fahrrad ohne Schuld ist wie ein Rad ohne Luft: Du kommst nicht weit, bis du dich darum kümmerst.",
    "Deine Schulden zu begleichen, ist wie ein Fahrrad zu ölen – es macht den Weg zur finanziellen Freiheit weniger quietschend.",
    "Schulden zurückzahlen ist wie ein Bergetappe auf dem Fahrrad: Es ist hart, aber die Aussicht oben ohne Ballast ist unbezahlbar.",
    "Wenn du deine Schulden wie dein Fahrrad behandelst – regelmäßig checken und pflegen –, wirst du nie ins Straucheln kommen.",
    "Bezahlen deine Schulden ist wie Radfahren gegen den Wind: Es fühlt sich anfangs schwer an, aber sobald du fertig bist, fühlst du dich wie ein Champion.",
    "Denk dran, Schulden zu machen ist leicht, genau wie Fahrradfahren bergab. Zurückzahlen? Nun, das ist der Bergauf-Teil!",
    "Schulden zu begleichen, ist wie das Finden der perfekten Gangschaltung: Es mag ein paar Versuche brauchen, aber sobald du es hinbekommst, läuft alles rund.",
    "Behandle deine Schulden wie ein altes Fahrrad: Nimm dir die Zeit, es Stück für Stück in Ordnung zu bringen, und bald fährt es sich wieder wie neu.",
    "Erinnerst du dich, wie aufregend es war, das Fahrradfahren zu lernen? Begleichen deine Schulden kann das gleiche Gefühl sein, nur dass du am Ende wegrollst, ohne finanzielle Schürfwunden.",
]


def remind_money_from_shift():
    shifts_df = load_shifts_from_file()
    if shifts_df.empty:
        return

    # get most recent entry from shifts_df where column "date" is in the past make sure to be timezone aware
    shifts_in_past = shifts_df[
        shifts_df["date"] < (datetime.now() - timedelta(days=1, hours=12))
    ]
    if shifts_in_past.empty:
        return

    last_shift = shifts_in_past.iloc[0]

    # get the names of all persons who were on duty
    shift_persons = set(
        last_shift[["primary", "secondary", "tertiary"]].dropna().tolist()
    )

    df_money = load_money_data()

    # filter df_money to only include entries from the last shift
    df_money = df_money[
        df_money["date"] >= last_shift["date"].tz_localize(settings.timezone)
    ]

    # filter only for positive values in df_money (i.e. debts)
    df_money = df_money[df_money["amount"] > 0]

    # get a list of all names in df_money
    df_money_names = set(df_money["name"].tolist())

    # see if any of the persons from the last shift are in df_money
    if shift_persons and not shift_persons.intersection(df_money_names):
        random_smileys = emojigen.get_random_emojis(12)
        send_message(
            f"Liebe(r) {', '.join(shift_persons)}, bitte trag ein wieviel Geld "
            f"beim letzten Dienst am {last_shift['date'].strftime('%d.%m.%Y')} mitgenommen wurde - Danke!\n"
            + "".join([x["image"] for x in random_smileys]),
            [settings.signal.internal_group],
        )


def remind_money_repayment():
    df = load_money_data()

    amount_sum_by_name = df.dropna(subset=["name"])

    # remove Vereinskonto and Lenkerbande Shop
    amount_sum_by_name = amount_sum_by_name[
        ~amount_sum_by_name["name"].isin(["Vereinskonto", "Lenkerbande Shop"])
    ]
    amount_sum_by_name = (
        amount_sum_by_name.groupby("name")["amount"].sum().sort_values(ascending=False)
    )

    max_debt = settings.money.max_debt

    too_high_debt = amount_sum_by_name[amount_sum_by_name.values > max_debt]

    if not too_high_debt.empty:
        send_message(
            f"Die folgenden Personen haben über {max_debt:0.0f} € Schulden: \n\n"
            + "\n".join([f"- {name}: {debt} €" for name, debt in too_high_debt.items()])
            + "\n\n"
            + f"🤑💵 {random.choice(repayment_sentences)} 💸💰"
            + "\n\n"
            + "In eigener Nachricht unten kommt die IBAN des Vereins",
            [settings.signal.internal_group],
        )
        send_message(
            settings.money.verein_iban,
            [settings.signal.internal_group],
        )


def send_reminders_money():
    """Send money reminders to internal group"""

    if not settings.reminder.file_path.exists():
        reminder_data = {}
    else:
        with settings.reminder.file_path.open("r") as file:
            reminder_data = json.load(file)

    today = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
    now_timestamp = today.timestamp()

    if (
        reminder_data.get("shift_money", 0)
        + settings.reminder.shift_money_days * 3600 * 24
        < now_timestamp
    ):
        remind_money_from_shift()
        reminder_data["shift_money"] = now_timestamp

    if (
        reminder_data.get("repayment", 0) + settings.reminder.repayment_days * 3600 * 24
        < now_timestamp
    ):
        remind_money_repayment()
        reminder_data["repayment"] = now_timestamp

    with settings.reminder.file_path.open("w") as file:
        json.dump(reminder_data, file)
