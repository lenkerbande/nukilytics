import datetime
import gettext
import json
import locale
import os
from enum import StrEnum
from gettext import gettext as _
from itertools import chain
from pathlib import Path
from typing import Any, Dict, List, Optional, Set

import numpy as np
import pandas as pd
import requests
import sentry_sdk
import streamlit as st
from git import InvalidGitRepositoryError, Repo
from pydantic import BaseModel, HttpUrl, model_validator, validator
from pydantic_settings import BaseSettings, SettingsConfigDict


class SignalSettings(BaseModel):
    data_dir_path: Path

    base_url: Optional[str] = None
    sender_number: Optional[str] = None
    internal_group: Optional[str] = None
    external_group: Optional[str] = None

    @property
    def send_url(self) -> str:
        return self.base_url + "/v2/send" if self.base_url else ""

    @property
    def receive_url(self) -> str:
        return (
            f"{self.base_url}/v1/receive/{self.sender_number}" if self.base_url else ""
        )


class ReminderSettings(BaseModel):
    data_dir_path: Path

    plenum_internal_days: int = 7
    plenum_external_days: int = 3
    plenum_base_wiki_url: str = "https://wiki.lenkerbande.at/books/absteige-plena/page/"
    cm_days: int = 1
    shift_money_days: int = 1
    repayment_days: int = 17

    @property
    def file_path(self) -> Path:
        return self.data_dir_path / "reminder.json"


class AuthSettings(BaseModel):
    data_dir_path: Path

    provider_base_url: Optional[HttpUrl] = None
    authorization_endpoint: str = ""
    token_endpoint: str = ""
    userinfo_endpoint: str = ""
    scope: str = "openid email profile"

    client_id: str = ""
    client_secret: str = ""

    # Required to retrieve users from authentik
    authentik_base_url: Optional[HttpUrl] = None
    authentik_token: str = ""

    board_group_name: str = "Vorstand"

    @property
    def members_file(self) -> Path:
        return self.data_dir_path / "members.json"

    @validator("authentik_base_url", always=True)
    def set_authentik_base_url(cls, v, values):
        return v or values.get("provider_base_url")

    @validator("authorization_endpoint", always=True)
    def set_authorization_endpoint(cls, v, values):
        return (
            v or str(values.get("provider_base_url", "")) + "application/o/authorize/"
        )

    @validator("token_endpoint", always=True)
    def set_token_endpoint(cls, v, values) -> str:
        return v or str(values.get("provider_base_url", "")) + "application/o/token/"

    @validator("userinfo_endpoint", always=True)
    def set_userinfo_endpoint(cls, v, values) -> str:
        return v or str(values.get("provider_base_url", "")) + "application/o/userinfo/"


class NukiSettings(BaseModel):
    data_dir_path: Path

    auth_header: Optional[str]
    smartlock_id: Optional[str]

    @property
    def file_path(self) -> Path:
        return Path(self.data_dir_path) / "nuki_data.csv"


class MoneySettings(BaseModel):
    data_dir_path: Path

    max_debt: float = 200

    verein_iban: str = "AT582011184128471600"

    bankproxy_url: str = "http://localhost:3000"

    absteige_creditors: List[str] = [
        "2., Harkortstrasse 7 / Immovision IV",
        "MASS Response Service GmbH",
        "oekostrom GmbH",
        "Juice Brothers",
    ]

    @property
    def money_file_path(self) -> Path:
        return Path(self.data_dir_path) / "money.csv"

    @property
    def bank_data_path(self) -> Path:
        return Path(self.data_dir_path) / "bank"

    @property
    def bank_accounts_path(self) -> Path:
        return Path(self.data_dir_path) / "bank_accounts.json"


class Settings(BaseSettings):
    model_config = SettingsConfigDict(env_nested_delimiter="__")

    sentry_dsn: str = ""

    timezone: str = "Europe/Berlin"
    name: str = "Absteige"
    default_language: str = "de"

    data_dir_path: Path = Path("data")

    days_of_shift: List[str] = ["wednesday"]

    extra_names: Set[str] = ["Vereinskonto"]
    blacklist_names: Set[str] = ["Keypad"]

    signal: Optional[SignalSettings] = None
    reminder: Optional[ReminderSettings] = None
    auth: Optional[AuthSettings] = None
    nuki: Optional[NukiSettings] = None
    money: Optional[MoneySettings] = None

    @property
    def shifts_file_path(self) -> Path:
        return Path(self.data_dir_path) / "shifts.csv"

    @property
    def names_json_path(self) -> Path:
        return Path(self.data_dir_path) / "names.json"

    @model_validator(mode="before")
    @classmethod
    def build_extra(cls, data) -> Dict[str, Any]:
        """Add data dir path to all configured sub-settings"""
        data_dir_path = data.get("data_dir_path") or Path("data")

        for key in ["signal", "reminder", "auth", "nuki", "money"]:
            data.setdefault(key, {})
            data[key]["data_dir_path"] = data_dir_path

        return data


settings = Settings()

if settings.sentry_dsn:
    sentry_sdk.init(
        dsn=settings.sentry_dsn,
        enable_tracing=False,
    )


# Mapping the action numbers to their names
action_mapping = {
    1: _("Unlock"),
    2: _("Lock"),
    3: _("Unlatch"),
    4: _("Lock'n'go"),
    5: _("Lock'n'go with unlatch"),
    208: "Door warning ajar",
    209: "Door warning status mismatch",
    224: "Doorbell recognition (only Opener)",
    240: _("Door opened"),
    241: _("Door closed"),
    242: "Door sensor jammed",
    243: "Firmware update",
    250: "Door log enabled",
    251: "Door log disabled",
    252: "Initialization",
    253: "Calibration",
    254: "Log enabled",
    255: "Log disabled",
    300: "Keypad unauthorized",
}

lock_actions = [2, 4, 5]
unlock_actions = [1, 3]
no_actions = [300]

door_opened = 240
door_closed = 241
door_actions = [door_closed, door_opened]


### Money
# enum for amount_types
class AmountType(StrEnum):
    DEBT = ":red[Debt]"
    CREDIT = ":green[Credit]"
    REPAYMENT = ":green[Repayment]"


class Category(StrEnum):
    DONATIONS = _("Donations")
    BEER = _("Beer")
    TOOLS = _("Tools/Stuff")
    CONSUMABLES = _("Consumables")
    OTHER = _("Other")
    RENT = _("Rent")


#### Localize

available_languages = {"de": "Deutsch", "en": "English"}


def set_language(language: str):
    global _
    global _n

    if language and language == "de":
        localizator = gettext.translation(
            "messages", localedir="locales", languages=[language]
        )
        localizator.install()
        _ = localizator.gettext
        _n = localizator.ngettext
        locale_str = "de_AT.UTF-8"
    else:
        _ = gettext.gettext
        _n = gettext.ngettext
        locale_str = "en_US.UTF-8"

    locale.setlocale(locale.LC_ALL, locale_str)

    # CookieManager.get_manager().set("language", language)


## Signal message sending


def send_message(message: str, recipients: list[str]):
    """Send message to recipients"""
    print(message)

    if not settings.signal.send_url or not message:
        return

    if not recipients or not recipients[0]:
        print("No recipients for message set")
        return

    response = requests.post(
        settings.signal.send_url,
        json={
            "recipients": recipients,
            "number": settings.signal.sender_number,
            "message": message,
        },
    )
    if not response.ok:
        print(f"Error sending message: {response.text}")


def load_access_data() -> pd.DataFrame:
    file_path = settings.nuki.file_path
    if not file_path.exists():
        return pd.Dataframe()

    df = pd.read_csv(str(file_path), parse_dates=["date"])
    # load date with timezone CET
    df["date"] = pd.to_datetime(df["date"], utc=True)
    # convert to CET
    df["date"] = df["date"].dt.tz_convert(settings.timezone)

    # remove keypad from name
    df["name"] = df["name"].str.replace("(Keypad)", "")
    # remove whitespaces from name
    df["name"] = df["name"].str.strip()
    df["action_name"] = df["action"].map(action_mapping)

    return df


def load_money_data():
    df = pd.read_csv(str(settings.money.money_file_path), parse_dates=["date"])
    # load timestamp with timezone CET
    df["timestamp"] = pd.to_datetime(df["timestamp"], utc=True, format="ISO8601")
    # convert to CET
    df["timestamp"] = df["timestamp"].dt.tz_convert(settings.timezone)

    df["date"] = pd.to_datetime(df["date"], utc=True, format="ISO8601")

    # make sure amount is float
    df["amount"] = df["amount"].astype(float)

    return df


def bank_csv_file_to_df(file: Path) -> pd.DataFrame:
    # read csv with utf-16 LE encoding
    # df = pd.read_csv(file, encoding="utf-16 LE", sep=";")
    df = pd.read_csv(file, sep=",")

    df["bookingDate"] = pd.to_datetime(df["bookingDate"], utc=True, format="%Y-%m-%d")

    # replace "." with "" in Betrag column
    # df["amount"] = df["amount"].str.replace(".", "")
    # replace "," with "." in Summe column
    # df["amount"] = df["amount"].str.replace(",", ".")
    df["amount"] = df["amount"].astype(float)

    return df


def load_bank_data(iban: str = None) -> pd.DataFrame:
    wildcard = f"*{iban}*.csv" if iban is not None else "*.csv"

    bank_csv_files = [
        bank_csv_file_to_df(file)
        for file in settings.money.bank_data_path.glob(wildcard)
    ]
    if not bank_csv_files or all(df is None for df in bank_csv_files):
        return

    bank_csv_files = [df.dropna(axis=1, how="all") for df in bank_csv_files]

    df = (
        pd.concat(bank_csv_files)
        .drop_duplicates()
        .sort_values(by="bookingDate", ascending=False)
    )

    # make "bookingDate" a date format
    df["Year"] = df["bookingDate"].dt.year
    df["bookingDate"] = df["bookingDate"].dt.date

    df = df.drop(columns={"Bezahlt mit", "BIC/SWIFT", "Bankleitzahl"} & set(df.columns))

    df["name"] = np.where(df["amount"] < 0, df["creditorName"], df["debtorName"])
    df["iban"] = np.where(df["amount"] < 0, df["creditorIban"], df["debtorIban"])

    return df


def date_select(
    df=None,
    date_start_default=None,
    date_end_default=None,
    show_quickselect: bool = False,
):
    if date_start_default is None:
        date_start_default = pd.to_datetime("now", utc=True) - pd.Timedelta(days=360)
    if date_end_default is None:
        date_end_default = pd.to_datetime("now", utc=True)

    if show_quickselect:
        col3, col1, col2 = st.columns(3)
        with col3:
            date_quickselect()
    else:
        col1, col2 = st.columns(2)

    if "date_start" not in st.session_state:
        st.session_state["date_start"] = date_start_default
    if "date_end" not in st.session_state:
        st.session_state["date_end"] = date_end_default

    date_start = col1.date_input(
        _("Start date"),
        key="date_start",
        value=st.session_state.get("date_start", date_start_default),
        disabled=bool(st.session_state.get("date_quickselect", False)),
    )
    date_end = col2.date_input(
        _("End date"),
        key="date_end",
        disabled=bool(st.session_state.get("date_quickselect", False)),
    )

    return pd.to_datetime(date_start, utc=True), pd.to_datetime(date_end, utc=True)


def date_quickselect():
    date_quickselect = st.selectbox(
        _("Date Quickselect"),
        (
            _("Last 30 days"),
            _("Last 60 days"),
            _("Last 90 days"),
            _("Last 180 days"),
            _("Last 365 days"),
            _("All"),
        ),
        key="date_quickselect",
        index=None,
    )

    days = None
    if date_quickselect == _("All"):
        days = 365 * 20
    elif date_quickselect:
        # extract number from date_quickselect
        days = int(date_quickselect.split(" ")[1])

    if days is not None:
        date_start = pd.to_datetime("now", utc=True) - pd.Timedelta(days=days)
        date_end = pd.to_datetime("now", utc=True)

        st.session_state["date_start"] = date_start
        st.session_state["date_end"] = date_end


def get_names(df=None, show_all=False):
    if df is None:
        df_money = load_money_data()
        df_access = load_access_data()

        if not show_all:
            # filter just last 90 days from df, date is a timestamp
            recent_date = pd.to_datetime(
                pd.Timestamp.now() - pd.Timedelta(days=90), utc=True
            )
            df_money = df_money[df_money["date"] > recent_date]
            df_access = df_access[df_access["date"] > recent_date]

        return sorted(
            set(
                pd.Series(np.concatenate((df_money["name"], df_access["name"])))
                .dropna()
                .unique()
            )
            - settings.blacklist_names
            | settings.extra_names
        )

    names = (
        set(df["name"].dropna().unique()) - settings.blacklist_names
        | settings.extra_names
    )
    return sorted(names)


def person_select(df=None):
    """Select from most recent persons"""

    col1, col2 = st.columns([5, 1])
    show_all = col2.checkbox(
        "all",
        value=False,
        help="show all",
        label_visibility="hidden",
    )

    names = get_names(df, show_all)

    my_name = st.session_state.get("username", None)
    my_name_index = None
    if my_name and my_name in names:
        # get index of my_name
        my_name_index = names.index(my_name)

    selected_name = col1.selectbox(
        _("Select a Person"),
        names,
        index=my_name_index,
        # label_visibility="collapsed",
        key="selected_name",
        help=_(
            "Only persons with recent activity are shown unless the checkbox is ticked"
        ),
    )

    return selected_name


def git_update_file(filepath: str, commit_message: str = None):
    """Update file in git repository"""
    data_dir_path = str(settings.data_dir_path)
    try:
        repo = Repo(data_dir_path)
    except InvalidGitRepositoryError:
        # if repo not initialized, initialize it
        repo = Repo.init(data_dir_path)

    repo.git.add(os.path.relpath(filepath, data_dir_path))

    # if no differences in git repo, then skip commit
    if not repo.is_dirty():
        return

    if not commit_message:
        commit_message = "Update of " + filepath
    repo.index.commit(commit_message)


def save_money_data(df, message: str = None):
    """Save given dataframe to csv file and send message to signal"""
    # save dataframe to csv
    money_file_path: Path = settings.money.money_file_path

    if money_file_path.is_file():
        df.to_csv(str(money_file_path), mode="a", header=False, index=False)
    else:
        df.to_csv(str(money_file_path), index=False)

    git_update_file(
        str(money_file_path),
        commit_message="Update money data by "
        + st.session_state.get("username", "unknown"),
    )

    # show success message
    if datetime.datetime.now().month in [12, 1, 2]:
        st.snow()
    else:
        st.balloons()

    with st.spinner(_("Sending message to Signal...")):
        send_message(message, [settings.signal.internal_group])


@st.cache_data(ttl="1h")
def get_users() -> list[dict]:
    """Get users from authentik API

    optional limit by groups
    """
    members_file = settings.auth.members_file

    # load from members_file if file is newer than 1 hour
    if (
        members_file.exists()
        and (datetime.datetime.now().timestamp() - members_file.stat().st_mtime) < 3600
    ):
        with members_file.open() as f:
            return json.load(f)

    auth_headers = {"Authorization": "Bearer " + settings.auth.authentik_token}
    page_size = 20
    users_url = (
        str(settings.auth.authentik_base_url)
        + f"/api/v3/core/users/?groups_by_name={settings.name}&page_size={page_size}"
    )

    response = requests.get(users_url, headers=auth_headers)
    results = response.json()["results"]

    # get all results from the API
    while next_page := response.json()["pagination"]["next"]:
        response = requests.get(users_url + f"&page={next_page}", headers=auth_headers)
        results += response.json()["results"]

    # write results to members file
    with members_file.open("w") as f:
        json.dump(results, f)

    return results


def get_user_names() -> list[str]:
    """Get user names from authentik API"""
    return [user["name"] for user in get_users()]


def get_user_banks() -> set[str]:
    """Get user bank accounts from authentik API"""
    users = get_users()
    bank_names = [u["attributes"]["bank"] for u in users if "bank" in u["attributes"]]
    bank_names = set(
        chain.from_iterable(
            [name] if isinstance(name, str) else name for name in bank_names
        )
    )

    bank_names |= {
        "Stephan Christopher Kruckenhauser",
        "Mag. Marcel Jira",
        "Dipl. Ing.Fabian Helm",
        "Koller Dipl.Ing.Erich",
        "Lina Schmidl",
        "Lukas Jahn",
        "Kai Julian Lingnau",
    }
    return bank_names


def load_shifts_from_file():
    shifts_file_path = settings.shifts_file_path
    if shifts_file_path.is_file():
        # load csv, column date as timestamp, other columns as strings
        df = pd.read_csv(
            str(shifts_file_path),
            parse_dates=["date"],
            dtype={"primary": str, "secondary": str, "tertiary": str},
        )

    else:
        df = pd.DataFrame(
            {
                "date": pd.Series(dtype="datetime64[ns]"),
                "primary": pd.Series(dtype="object"),
                "secondary": pd.Series(dtype="object"),
                "tertiary": pd.Series(dtype="object"),
            }
        )
    return df
