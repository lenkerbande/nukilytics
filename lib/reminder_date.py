# plenums-reminder

# Path: lib/reminder_date.py

import json
from datetime import datetime, timedelta

from lib.common import send_message, settings


def get_first_weekday(date: datetime, weekday: int) -> datetime:
    """Get the first weekday of a month"""
    first_day = date.replace(day=1)
    return first_day + timedelta(days=(weekday - first_day.weekday() + 7) % 7)


def send_reminders_date():
    """Send plenum reminder to internal and external group chat"""

    if not settings.reminder.file_path.exists():
        reminder_data = {}
    else:
        with settings.reminder.file_path.open("r") as file:
            reminder_data = json.load(file)

    today = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
    now_timestamp = today.timestamp()

    next_plenum = get_first_weekday(today, 1)
    if next_plenum < today:
        next_plenum = get_first_weekday(today + timedelta(days=32), 1)

    days_to_plenum = round((next_plenum.timestamp() - now_timestamp) / (60 * 60 * 24))
    plenum_date_str = next_plenum.strftime("%d.%m.%Y")

    reminder_to_send = {}

    if (
        days_to_plenum <= settings.reminder.plenum_internal_days
        and reminder_data.get("internal", 0) < next_plenum.timestamp()
    ):
        reminder_to_send["internal"] = (
            (
                f"Am {plenum_date_str} is wieda Plenum, bitte Agenda erstellen: "
                f"{settings.reminder.plenum_base_wiki_url}{next_plenum.strftime('%Y-%m-%d')}"
            ),
            [settings.signal.internal_group],
            next_plenum.timestamp(),
        )

    if (
        days_to_plenum <= settings.reminder.plenum_external_days
        and reminder_data.get("external", 0) < next_plenum.timestamp()
    ):
        reminder_to_send["external"] = (
            (
                f"Am {plenum_date_str} is Plenum 💬🧑‍🤝‍🧑🚲, hier die Agenda: "
                f"{settings.reminder.plenum_base_wiki_url}{next_plenum.strftime('%Y-%m-%d')}"
            ),
            [settings.signal.external_group],
            next_plenum.timestamp(),
        )

    # cm is always each third friday of a month
    next_cm = get_first_weekday(today, 4) + timedelta(weeks=2)
    if next_cm < today:
        next_cm = get_first_weekday(today + timedelta(days=32), 4) + timedelta(weeks=2)

    days_to_cm = round((next_cm.timestamp() - now_timestamp) / (60 * 60 * 24))

    if days_to_cm <= settings.reminder.cm_days and reminder_data.get("cm", 0) < (
        next_cm.timestamp()
    ):
        reminder_to_send["cm"] = (
            (
                f"Am {next_cm.strftime('%d.%m.%Y')} is wieder critical mass 🚲🚲🚲 "
                f"Abfahrt 16.30h bei der Abs, Treffpunkt 17h Schwarzenbergplatz 🚲🚲🚲"
            ),
            [settings.signal.external_group],
            next_cm.timestamp(),
        )

    for key, (message, recipients, timestamp) in reminder_to_send.items():
        send_message(message, recipients)
        reminder_data[key] = timestamp

    with settings.reminder.file_path.open("w") as file:
        json.dump(reminder_data, file)
