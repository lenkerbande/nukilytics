import time
from gettext import gettext as _

import streamlit as st
from streamlit_cookies_controller import CookieController
from streamlit_js_eval import get_browser_language

from lib.common import set_language, settings


def menu():
    controller = None
    if "user_data" not in st.session_state:
        st.session_state.controller = CookieController()
        time.sleep(0.5)

    language = st.session_state.language = st.session_state.get(
        "language", (get_browser_language() or settings.default_language)[0:2]
    )

    set_language(language)

    st.sidebar.page_link("app.py", label=_("🏠 Home"))
    st.sidebar.page_link("pages/Shifts_List.py", label=_("💪 Shifts List"))
    st.sidebar.page_link("pages/Money_Form.py", label=_("💰 Money Form"))
    st.sidebar.page_link("pages/Beer_Form.py", label=_("🍺 Beer Form"))
    st.sidebar.page_link("pages/Debts.py", label=_("💸 Debts"))
    st.sidebar.page_link("pages/Access_Analytics.py", label=_("📈 Access Analytics"))

    user_data = st.session_state.get("user_data")
    if user_data and settings.auth.board_group_name in user_data.get("groups", []):
        st.sidebar.page_link("pages/Money_Analytics.py", label=_("🤑 Money Analytics"))
        st.sidebar.page_link("pages/Members.py", label=_("👯 Members"))
        st.sidebar.page_link("pages/Bank_Account.py", label=_("🏦 Bank Account"))
