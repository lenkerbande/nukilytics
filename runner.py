import argparse
import datetime

import requests

from lib.common import settings
from lib.nuki_datafetcher import update_data
from lib.reminder_date import send_reminders_date
from lib.reminder_money import send_reminders_money


def main():
    # Create the parser
    parser = argparse.ArgumentParser(description="Run a script.")

    # Add optional arguments
    parser.add_argument(
        "--update-data", "-u", action="store_true", help="Run update nuki data"
    )
    parser.add_argument(
        "--send-reminder-date",
        "-s",
        action="store_true",
        help="Send plenum date reminder",
    )
    parser.add_argument(
        "--send-reminder-money", "-m", action="store_true", help="Send money reminders"
    )

    # Parse the arguments
    args = parser.parse_args()

    # if signal is configured, make sure to always fetch latest messages regularily
    if settings.signal.receive_url:
        response = requests.get(settings.signal.receive_url)
        if response.ok:
            print("Received messages from signal")
        else:
            print(f"Error receiving messages from signal: {response.text}")

    if args.update_data:
        update_data()

    # only send reminder out of sleeping hours
    if not (8 < datetime.datetime.now().hour < 20):
        return

    if args.send_reminder_date:
        send_reminders_date()

    if args.send_reminder_money:
        send_reminders_money()


if __name__ == "__main__":
    main()
