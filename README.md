# Nukilytics

Analytics tool for nuki logs

Split in two parts:

- nuki_datafetcher: loads data from nuki api
- nuki_analytics: shows fetched data with streamlit

## Update translations

`make update_po`

## Upgrade packages

`uv sync -U`
